﻿--データベース生成--
CREATE DATABASE IF NOT EXISTS `myweb_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;

--ユーザー情報テーブル生成--
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_password` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phoneNo` int(11) DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--商品情報テーブル--
CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `gender`int(11) DEFAULT NULL,
  `sale`int(11) DEFAULT NULL,
  `file_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
);

--購入情報テーブル--
CREATE TABLE `t_buy_detail` (
  `id` int(11) NOT NULL,
  `buy_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL
  );
  
--購入詳細テーブル--
 CREATE TABLE `t_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
   PRIMARY KEY (`id`)
);

INSERT INTO `t_user` (`name`, `user_id`, `login_password`, `address`, `email`,`phoneNo`,`create_date`) VALUES('おもち','omochi','1234','東京都ああ','omochi@gmail.com','123456789',NOW());

SELECT * FROM t_user WHERE user_id = omochi, and login_password = 1234;

--商品追加ーー
INSERT INTO `item` (`id`,`name`,`detail`,`price`,`gender`,`sale`,`file_name`) VALUES
(1,'men01','いい',10000,1,0,'82621-1.png'),
(2,'men02','いいね',15000,1,0,'002.png'),
(3,'men03','いいよ',20000,1,0,'46150180.png'),
(4,'men04','いいわ',30000,1,0,'b80e50c4.png'),
(5,'men05','いいよね',10000,1,0,'cameron.png'),
(6,'men06','いいい',20000,1,0,'fs4545_1.png'),
(7,'men07','おしゃれ',10000,1,0,'fs4708_1.png'),
(8,'men08','おしゃん',15000,1,0,'.fs4784-1png'),
(9,'men09','おされ',30000,1,0,'jr1157.png'),
(10,'men10','おささ',15000,1,0,'.jr1354.png'),
(11,'men11','おしゃれ時計',20000,1,0,'jr1355.png'),
(12,'men12','ええ',15000,1,0,'jr1355_1.png'),
(13,'men13','ええな',20000,1,0,'me1001b.png'),
(14,'men14','ええやん',40000,1,0,'me3008.png'),
(15,'men15','かっこいい',20000,1,0,'me3044.png'),
(16,'men16','かちょいい',15000,1,30,'me3098.png'),
(17,'men17','かこいい',20000,1,40,'me3099.png'),
(18,'men18','カッコイイ',25000,1,30,'me3102.png'),
(19,'men19','よい',35000,1,50,'th.png'),
(20,'men20','よよ',40000,1,20,'0172.png');

INSERT INTO `item` (`id`,`name`,`detail`,`price`,`gender`,`sale`,`file_name`) VALUES
(21,'lad01','いい',10000,2,0,'0423.png'),
(22,'lad02','いいね',15000,2,0,'69327.png'),
(23,'lad03','いいよ',20000,2,0,'110282077.png'),
(24,'lad04','いいわ',30000,2,0,'ch2795.png'),
(25,'lad05','いいよね',10000,2,0,'ch2891.png'),
(26,'lad06','いいい',20000,2,0,'d03c.png'),
(27,'lad07','おしゃれ',10000,2,0,'es2859.png'),
(28,'lad08','おしゃん',15000,2,0,'es3060.png'),
(29,'lad09','おされ',30000,2,20,'es3077.png'),
(30,'lad10','おささ',15000,2,30,'es3262.png'),
(31,'lad11','おしゃれ時計',20000,2,30,'es4645.png'),
(32,'lad12','ええ',15000,2,40,'fs4552.png'),
(33,'lad13','ええな',20000,2,50,'ftw1106.png'),
(34,'lad14','ええやん',40000,2,70,'th1.png'),
(35,'lad15','かっこいい',20000,2,70,'th2.png');

SELECT * FROM item WHERE id = 1;

INSERT INTO t_buy(user_id,item_id,create_date) VALUES(1,20,NOW());

SELECT * FROM t_buy JOIN item ON t_buy.item_id = item.id WHERE t_buy.user_id = 5;

SELECT * FROM t_buy JOIN item ON t_buy.item_id = item.id WHERE t_buy.user_id = 5;

SELECT * FROM item WHERE name LIKE '%0%';
