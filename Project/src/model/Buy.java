package model;

import java.io.Serializable;
import java.util.Date;

	public class Buy implements Serializable {
		private int id;
		private int userId;
		private int itemId;
		private Date create_date;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}

		public int ItemId() {
			return itemId;
		}
		public void setItemId(int itemId) {
			this.itemId = itemId;
		}

		public Date getCreate_date() {
			return create_date;
		}
		public void setCreate_date(Date create_date) {
			this.create_date = create_date;
		}

}
