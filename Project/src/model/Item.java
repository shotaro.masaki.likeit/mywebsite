package model;

import java.io.Serializable;

public class Item implements Serializable {
		private int id;
		private String name;
		private String detail;
		private int price;
		private int gender;
		private int sale;
		private String file_name;

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
			}
		public void setName(String name) {
			this.name = name;
		}

		public String getDetail() {
			return detail;
		}
		public void setDetail(String detail) {
			this.detail = detail;
		}

		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}

		public int getGender() {
			return gender;
		}
		public void setGender(int gender) {
			this.gender = gender;
		}

		public int getSale() {
			return sale;
		}
		public void setSale(int sale) {
			this.sale= sale;
		}

		public String getFile_name() {
			return file_name;
		}
		public void setFile_name(String file_name) {
			this.file_name = file_name;
		}
}
