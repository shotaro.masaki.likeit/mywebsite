package model;

import java.io.Serializable;

	public class User implements Serializable {
		private int id;
		private String name;
		private String userId;
		private String password;
		private String address;
		private String email;
		private String phoneNo;
		private String createDate;

		public User(String userId, String name ) {
			this.userId = userId;
			this.name = name;
		}

		public User(int id, String name, String userId, String password, String address, String phoneNo,String createDate) {
			this.id = id;
			this.userId = userId;
			this.name = name;
			this.address = address;
			this.password = password;
			this.createDate = createDate;
			this.phoneNo = phoneNo;
		}

		public User(String userId, String name, String address, String email, String phoneNo, String createDate) {

			this.userId = userId;
			this.name = name;
			this.address = address;
			this.email = email;
			this.phoneNo = phoneNo;
			this.createDate= createDate;
		}


		public User(String userId, String name, int id) {
			this.userId = userId;
			this.name = name;
			this.id = id;
		}

		public User(String name, String address, String email, String phoneNo) {
			this.name = name;
			this.address = address;
			this.email = email;
			this.phoneNo = phoneNo;
		}

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getCreateDate() {
			return createDate;
		}
		public void setCreateDate(String createDate) {
			this.createDate = createDate;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPhoneNo() {
			return phoneNo;
		}
		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}


}
