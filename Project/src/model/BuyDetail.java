package model;

import java.io.Serializable;

	public class BuyDetail implements Serializable {
		private int id;
		private int userId;
		private int itemId;
		private String create_date;
		private String name;
		private int price;


		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}

		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}

		public int getItemId() {
			return itemId;
		}
		public void setItemId(int itemId) {
			this.itemId = itemId;
		}

		public String getCreate_date() {
			return create_date;
		}
		public void setCreate_date(String create_date) {
			this.create_date = create_date;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}

		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
}
