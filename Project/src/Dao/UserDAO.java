package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDAO {
	// インスタンスオブジェクトを返却させてコードの簡略化

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String userId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			password = Encrypt(password);


			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE user_id = ? and login_password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("user_id");
			String nameData = rs.getString("name");
			int Id = rs.getInt("id");
			return new User(loginIdData, nameData,Id);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * IDに紐づくユーザ情報を返す
	 * @return
	 */
	public User findByUserInfo(String Id) {
				Connection conn = null;
				try {
					// データベースへ接続
					conn = DBmanager.getConnection();

					// SELECT文を準備
					String sql = "SELECT * FROM t_user WHERE id = ?";

					// SELECTを実行し、結果表を取得
					PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1, Id);
					ResultSet rs = pStmt.executeQuery();

					if (!rs.next()) {
						return null;
					}

					// 必要なデータのみインスタンスのフィールドに追加
					String UserId = rs.getString("user_id");
					String name = rs.getString("name");
					String Address = rs.getString("address");
					String Email = rs.getString("email");
					String PhoneNo = rs.getString("phoneNo");
					String CreateDate = rs.getString("create_date");
					return new User(UserId, name, Address, Email, PhoneNo,CreateDate);

				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}

					}
		}
	}

	public User findByUserUpdate(int id) {
		Connection conn = null;


		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加

			String name = rs.getString("name");
			String Address = rs.getString("address");
			String Email = rs.getString("email");
			String PhoneNo = rs.getString("phoneNo");

			return new User(name, Address, Email, PhoneNo);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void Update(String name, String address, String email, String phoneNo, String Id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// UPDATE文を準備
			String sql = "UPDATE t_user SET name = ?,address = ?, email = ?, phoneNo = ? WHERE id = ?";

			// UPDATE文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, address);
			pStmt.setString(3, email);
			pStmt.setString(4, phoneNo);
			pStmt.setString(5, Id);

			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

	}

	/**
	 * UserIdに紐づき、Idの重複を防ぐ
	 */
	public String findByUserEntry(String UserId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM t_user WHERE user_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, UserId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String UserloginId = rs.getString("user_id");

			return UserloginId;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ユーザー新規登録用
	 */
	public void CreateUser(String name, String userId, String password, String address, String email, String phoneNo ) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();
			password = Encrypt(password);

			// INSERT文を準備
			String sql = "INSERT INTO t_user(name,user_id,login_password,address,email,phoneNo,create_date)VALUES(?,?,?,?,?,?,NOW())";

			// INSERT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, userId);
			pStmt.setString(3, password);
			pStmt.setString(4, address);
			pStmt.setString(5, email);
			pStmt.setString(6, phoneNo);
			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public String Encrypt(String password) {

			//ハッシュを生成したい元の文字列
			String result=null;
			String source = password;

			try {
				//ハッシュ生成前にバイト配列に置き換える際のCharset
				Charset charset = StandardCharsets.UTF_8;
				//ハッシュアルゴリズム
				String algorithm = "MD5";

				//ハッシュ生成処理
				byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				 result = DatatypeConverter.printHexBinary(bytes);

				//標準出力
				System.out.println(result);

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();

			}
			return result;
	}
}
