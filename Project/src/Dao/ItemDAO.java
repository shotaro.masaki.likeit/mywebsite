package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Item;

public class ItemDAO {

	/**
	 * 全商品検索
	 */
	public List<Item> findAll() throws SQLException {
		Connection conn = null;
		List<Item> itemList = new ArrayList<Item>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM item";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Item item = new Item();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFile_name(rs.getString("file_name"));

				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}


/**
 * メンズ商品検索
 */
public List<Item> findbygender1() throws SQLException {
	Connection conn = null;
	List<Item> itemList = new ArrayList<Item>();

	try {
		// データベースへ接続
		conn = DBmanager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM item WHERE gender = 1";

		// SELECTを実行し、結果表を取得
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			Item item = new Item();
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setFile_name(rs.getString("file_name"));

			itemList.add(item);
		}

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return itemList;
}
/**
 * レディース商品検索
 */
public List<Item> findbygender2() throws SQLException {
	Connection conn = null;
	List<Item> itemList = new ArrayList<Item>();

	try {
		// データベースへ接続
		conn = DBmanager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM item WHERE gender = 2";

		// SELECTを実行し、結果表を取得
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			Item item = new Item();
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setFile_name(rs.getString("file_name"));

			itemList.add(item);
		}

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return itemList;
}
/**
 * itemidによる商品検索
 */
public static Item getItemByItemID(int itemId) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {
		con = DBmanager.getConnection();

		st = con.prepareStatement("SELECT * FROM item WHERE id = ?");
		st.setInt(1, itemId);

		ResultSet rs = st.executeQuery();

		Item item = new Item();
		if (rs.next()) {
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setFile_name(rs.getString("file_name"));
		}

		System.out.println("searching item by itemID has been completed");

		return item;
	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
	}
	/**
	 * sale品のみの商品検索
	 */

public List<Item> findbysaleItem() throws SQLException {
	Connection conn = null;
	List<Item> itemList = new ArrayList<Item>();

	try {
		// データベースへ接続
		conn = DBmanager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM item WHERE sale != 0";

		// SELECTを実行し、結果表を取得
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			Item item = new Item();
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setFile_name(rs.getString("file_name"));
			int Price = rs.getInt("price");
			int Sale = rs.getInt("sale");
			int SalePrice = Price - Price*Sale/100;

			item.setPrice(SalePrice);

			itemList.add(item);
		}

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return itemList;
}

/**
 * 検索フォームからのサーチ機能
 */
public List<Item> getItembyItemName(String searchword) throws SQLException {
	Connection conn = null;
	PreparedStatement st = null;

	try {
		// データベースへ接続
		conn = DBmanager.getConnection();

		if (searchword.length() == 0) {
			// 全検索
			st = conn.prepareStatement("SELECT * FROM item ");

		} else {
			// 商品名検索
			st = conn.prepareStatement("SELECT * FROM item WHERE name LIKE ? ");
			st.setString(1,"%" + searchword + "%");

		}

		ResultSet rs = st.executeQuery();
		List<Item> itemList = new ArrayList<Item>();

		while (rs.next()) {
			Item item = new Item();
			item.setId(rs.getInt("id"));
			item.setName(rs.getString("name"));
			item.setDetail(rs.getString("detail"));
			item.setPrice(rs.getInt("price"));
			item.setFile_name(rs.getString("file_name"));

			itemList.add(item);
		}
		return itemList;

	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (conn != null) {
			conn.close();
		}
	}
}

}


