package Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import model.BuyDetail;

public class BuyDAO {

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 *
	 * @param session
	 * @param str
	 * @return
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * 購入情報登録処理
	 * @param bdb 購入情報
	 * @throws SQLException 呼び出し元にスローさせるため
	 */
	public void insertBuy(String userId,String itemId) throws SQLException {
		Connection conn = null;

		try {
			conn = DBmanager.getConnection();

			String sql = "INSERT INTO t_buy(user_id,item_id,create_date) VALUES(?,?,NOW())";


			// INSERT文を実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userId);
			pStmt.setString(2, itemId);
			pStmt.executeUpdate();

			pStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		}

		/**
		 * ユーザーIdに紐づいた購入商品検索
		 *
		 */

		public static List<BuyDetail> getBuyDetailByUserId(String Id) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			List<BuyDetail> bdList = new ArrayList<BuyDetail>();

			try {
				con = DBmanager.getConnection();

				st = con.prepareStatement(
						"SELECT * FROM t_buy JOIN item ON t_buy.item_id = item.id WHERE t_buy.user_id = ?");
				st.setString(1, Id);

				ResultSet rs = st.executeQuery();

				while (rs.next()) {
					BuyDetail bd = new BuyDetail();
					bd.setId(rs.getInt("id"));
					bd.setName(rs.getString("name"));
					bd.setPrice(rs.getInt("price"));
					bd.setCreate_date(rs.getString("create_date"));

					bdList.add(bd);

				}

				System.out.println("searching BuyDataBeans by buyID has been completed");

			} catch (SQLException e) {

				System.out.println(e.getMessage());
				throw new SQLException(e);

			} finally {
				if (con != null) {
					con.close();
				}

			}
			return bdList;

	}
}
