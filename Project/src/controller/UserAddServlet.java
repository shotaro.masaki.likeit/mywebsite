package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User Id = (User)session.getAttribute("userdetail");

		if(Id != null) {

		// ユーザ一覧のjspにリダイレクト
		response.sendRedirect("LoginServlet");
		return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//文字化け防止
		 request.setCharacterEncoding("UTF-8");

		 String name = request.getParameter("name");
		 String userId = request.getParameter("userId");
		 String password = request.getParameter("password");
		 String confirmpassword = request.getParameter("confirmpassword");
		 String address = request.getParameter("address");
		 String email = request.getParameter("email");
		 String phoneNo = request.getParameter("phoneNo");

		 UserDAO userdao = new UserDAO();
		 String user = userdao.findByUserEntry(userId);

		//パスワードが違う場合
		 if  (!password.equals(confirmpassword)) {
			 request.setAttribute("errMsg", "パスワードが異なります。");
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
				dispatcher.forward(request, response);
				return;
		 //空欄が生まれた場合
		 }else if(name.equals("") || userId.equals("") || password.equals("") || confirmpassword.equals("") || phoneNo.equals("") || address.equals("") || email.equals("")) {
			 request.setAttribute("errMsg", "空白が存在します。");
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
				dispatcher.forward(request, response);
				return;
		//ログインIDが重複した場合
		 }else if(user != null) {
			request.setAttribute("errMsg", "すでに存在するIDです。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
				dispatcher.forward(request, response);
				return;
		 }

		 //リダイレクト
		 UserDAO userDao = new UserDAO();
		 userDao.CreateUser(name,userId,password,address,email,phoneNo);

		 //ログイン処理も行う。



		 response.sendRedirect("LoginServlet");



	}

}
