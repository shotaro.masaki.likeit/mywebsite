package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.BuyDAO;
import model.Item;
import model.User;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetailServlet")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			int Id = Integer.parseInt(request.getParameter("item_id"));


			//対象のアイテム情報を取得
			Item item = Dao.ItemDAO.getItemByItemID(Id);
			//リクエストパラメーターにセット
			request.setAttribute("item", item);

			//アイテム詳細にフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemdetail.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
	}
	}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User Id = (User)session.getAttribute("userdetail");
		String ItemId = request.getParameter("item_id");

		if(Id == null) {

			// ユーザー登録画面にリダイレクト
			response.sendRedirect("LoginServlet");
			//二重を防ぐため
			return;
			}

		try {

		//文字化け防止
	    request.setCharacterEncoding("UTF-8");

	    // IDを受け取る
		String UserId = request.getParameter("id");

		BuyDAO buy = new BuyDAO();
		buy.insertBuy(UserId, ItemId);

		// 購入完了画面にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/buyresult.jsp");
		dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}



	}

}
