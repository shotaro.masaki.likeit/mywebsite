package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User Id = (User)session.getAttribute("userdetail");

		if(Id == null) {

		// ユーザー登録画面にリダイレクト
		response.sendRedirect("UserAddServlet");
		//二重を防ぐため
		return;
		}

		//文字化け防止
		    request.setCharacterEncoding("UTF-8");

		// IDを受け取る
			String UserId = request.getParameter("id");


		//Daoメソッドの実行
			UserDAO userDao = new UserDAO();
			User user = userDao.findByUserInfo(UserId);

		//リクエストスコープにセット

			request.setAttribute("user", user);

		//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userdetail.jsp");
			dispatcher.forward(request, response);




	}

}
