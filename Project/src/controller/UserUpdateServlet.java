package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDAO;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User Id = (User)session.getAttribute("userdetail");

		if(Id == null) {

		// メイン画面にリダイレクト
		response.sendRedirect("LoginServlet");
		return;
		}
    	//文字化け防止
		 request.setCharacterEncoding("UTF-8");

		// IDを受け取る
		 int UserId = Id.getId();

		 UserDAO userDao = new UserDAO();
		 User user = userDao.findByUserUpdate(UserId);

			request.setAttribute("user",user);

		//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//文字化け防止
		 request.setCharacterEncoding("UTF-8");

		 String Id = request.getParameter("id");
		 String name = request.getParameter("name");
		 String address = request.getParameter("address");
		 String email = request.getParameter("email");
		 String phoneNo = request.getParameter("phoneNo");


		 //いずれかが空欄のとき、再入力
		 if(name.equals("") || address.equals("") || email.equals("") || phoneNo.equals("")) {
			 request.setAttribute("errMsg", "空白が存在します。");
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
				dispatcher.forward(request, response);
				return;

		 }

		 //DAOの実行
		 	UserDAO userdao = new UserDAO();
		 	userdao.Update(name, address, email, phoneNo,Id);

		 //メイン画面にリダイレクト
		 	response.sendRedirect("MainHomeServlet");

	}

}
