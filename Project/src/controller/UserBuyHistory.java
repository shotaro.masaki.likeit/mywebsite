package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.BuyDAO;
import model.BuyDetail;
import model.User;

/**
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyHistory() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User Id = (User)session.getAttribute("userdetail");

		try {
		if(Id == null) {

		// ユーザー登録画面にリダイレクト
		response.sendRedirect("UserAddServlet");
		//二重を防ぐため
		return;
		}

		//文字化け防止
		    request.setCharacterEncoding("UTF-8");

		// IDを受け取る
			String UserId = request.getParameter("id");

		//Daoメソッドの実行
			List<BuyDetail> bd = BuyDAO.getBuyDetailByUserId(UserId);

		//何も入っていないときは、共通ホームに遷移
			if(bd == null) {

				// ユーザー登録画面にリダイレクト
				response.sendRedirect("MainHomeServlet");
				return;

				}




		//リクエストスコープにセット

			request.setAttribute("bd", bd);

		//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyhistory.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
