package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Dao.UserDAO;
import model.User;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがある場合、表通ホーム画面にリダイレクトさせる
				HttpSession session = request.getSession();
				User Id = (User)session.getAttribute("mainhome");

				if(Id != null) {

				// 共通ホームのjspにリダイレクト
				response.sendRedirect("mainhome");
				return;//リダイレクトとフォワードの二重処理を防ぐ
			}

				// フォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
			request.setCharacterEncoding("UTF-8");

		//パラメーターから取得
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			UserDAO userdao = new UserDAO();
			User user = userdao.findByLoginInfo(loginId, password);

		/** テーブルに該当のデータが見つからなかった場合 **/
			if (user == null) {
		// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");

		// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
			}

		/** テーブルに該当のデータが見つかった場合 **/
		// セッションにユーザの情報をセット
			HttpSession session = request.getSession();
			session.setAttribute("userdetail", user);


		// 共通ホームにリダイレクト
			 response.sendRedirect("MainHomeServlet");


	}

}
